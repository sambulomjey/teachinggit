# TeachingGit README
This repo contains a series of markdown files with basic instructions for using git.  This README here acts as an index.

Note: Some contents here are adapted from [SoftwareCarpentry GitNovice](https://swcarpentry.github.io/git-novice/).

1. [GettingStarted](GettingStarted.md): For instructions on installing and configuring the necessary tools in various operating systems.
    * [Bash in 2 slides](bash.md)
2. [Initialise a Local Repository](LocalRepo.md)
3. [Tracking changes](TrackChanges.md)
4. [Branching and merging - an intro](Branch.md)
5. [Undoing changes](Undo.md)
6. [Remotes](Remotes.md)


