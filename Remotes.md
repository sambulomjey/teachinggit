# Remotes:
1. Signup for a [Gitlab account](https://gitlab.com/)
2. Create a demo repo using the web browser interface and clone it to your laptop
    * Create it using the [webgui](https://gitlab.com/projects/new), selecting blank project and follow the intructions shown
    * A note on git clone: git clone can be used to 
        - Clone locally using the file path to the repo folder: git clone /home/username/myorigonalrepo
        - clone over https.  In this case the protocol is smart enough to ask for your username and password to authenticate
        - Clone over ssh.  Primary advanatge being the use of keys (passwordless authentication)
        - To use an ssh key see [here](https://www.atlassian.com/git/tutorials/git-ssh) for how to generate one on your OS, then add it to your gitlab account under preferences.
    * Note the same process caters for creating a new repo online, and pushing an existing project to gitlab
```bash
    $ git clone <url>
```

3. Once you have a new project(repo) on gitlab and cloned to your laptop make some changes and a commit them and **push** the change to the remote (Gitlab) copy of the repo
```bash
    # Have a look at where you're pushing to 
    $ git remote -v
    $ git push
```

4. Now on gitlab in the webgui make some more changes and then git **pull** those changes down to your laptop.  
```bash
    $ git pull
```

5. Again make some changes on gitlab in the webgui then git **fetch** those changes down to your laptop, and look at the differences between the remote and your local, this is a safe way of checking what is coming down before you merge it.  More precisely, git pull runs git fetch with the given parameters and calls git merge to merge the retrieved branch
```bash
    $ git fetch
```

6. Finally, simulate the whole complex git flow example as follows:
    a. Create a new local feature branch and make some commits to it, to keep things simple and avoid conflicts for now, add a new file and make edits to that.
    ```bash
    $ git checkout -b newfeature
    ...edits and commits
    ```
    b. In the webgui on master make some more commits.  Again, to keep it easy make the edits in a file that newfeature doesn't make any changes to.

    c. On your local master branch pull in the remote changes
    ```bash
    $ git checkout master
    $ git pull
    ```
    d. On your local new feature branch rebase asgainst local master which now contains all the remote updates.  Look at the log to see the reordering of commits
    ```bash
    $ git checkout newfeature
    $ git rebase master
    ```

    e. From local newfeature branch which now has all the other remote master commits in it too you want to push this feature branch to remote and create a merge request
    ```bash
    $ git push -u origin newfeature
    ```
    f. On the webgui go to merge requests and approve your own merge request and then perform the merge.  You can also navigate to this page by clicking on the link that git gives you when you make the push in (e)

