# Tracking changes
1. If you want to track changes in a file tell git to do so by adding it. Now git will always assume it should care about changes to this file unless you tell it to stop caring.  Adding a file is also used to 'stage' changes.  To unstage a change or file use 'reset'

```bash
$ git add MyFile.txt
$ git reset MyFile.txt
```

2. Now that git knows you want to track that file, it will tell you if it changes when you query status.  To save changes made to a file to the git repository (and therefore make it possible to go back to these changes) commit the changes.
```bash
$ git commit -m "A useful commit message about what changes were made and why"
```

To both stage changes of tracked files and commit these use:
```bash
$ git commit -am "Useful changes"
```

To stop tracking a file remove it from the git cache.  Note this does not delete the file from your folder but if you leave out the cacched flag it will delete the file.  You probably won't do this often
```bash
$ git rm --cached filename
```

3. Every commit is a snapshot in the state of your documents. This state can be returned to.  To review changes and return a file to its state in a previous commit look at the logs.  Try each of the following in a git repo that has at least 3 commits already.
```bash
$ git log 
$ git log -2
$ git log --oneline
```
4. It is good practice to review staged changes before commiting
```bash
$ git diff filename
```
Changes in all tracked files in a repo that are currently unstaged
```bash
$ git diff 
```
Changes relative to a different commit
```bash
$ git diff commit_hash 
```


5. It is useful to be able to tell git to ignore some files.  Create a file called .gitignore
A bunch of useful pre-defined gitignore files are available [here](https://github.com/github/gitignore)


