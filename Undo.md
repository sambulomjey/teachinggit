# Undoing changes

1. Revert
Go back to a previous commit.  Creates a new commit with a new commit message recording the fact that the repo has been changed to now contain the contents as the were in the previous commit
```bash
$ git revert HEAD
$ git revert ###
$ git revert ### --no-commit //To only stage the inversion not create a new commit instantly
```

2. Checkout:
Checks out a previous commit, either of the whole repo or of a specific file.  You can do this to perhaps just look at a previous version but you'll be in a detatched HEAD state so shouldn't do further development
```bash
$ git checkout <file/hasg>
```

Rather, if you want to use and move forwards with this old state, checkout and make a new branch from where you can move forward and possibly later merge with the main trunk.
```bash
$ git checkout -b GoingBackToOldCOmmit ####
```

